<?php

namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\StoreEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Show the form for contacting the owner.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("contact");
    }

    /**
     * Store a newly created email.
     *
     * @param \App\Http\Requests\StoreEmail $request
     * @param \App\Email $email
     * @return array
     */
    public function store(StoreEmail $request, Email $email)
    {
        /** @var $email \App\Email|\Illuminate\Database\Eloquent\Model */
        $email = new $email;
            $email->sender_name  = $request->input('sender_name');
            $email->sender_email = $request->input('sender_email');
            $email->sender_phone = $request->input('sender_phone', null);
            $email->body         = $request->input('body');
        $success = $email->save();

        if ($success) {
            Mail::raw($request->input('body'), function ($messge) {
                $messge->to(env('OWNERS_EMAIL'));
            });
        }

        return [
            'success' => boolval($success),
            'message' => $success ? 'Email successfully sent off.' : 'that was unexpected...'
        ];
    }
}
