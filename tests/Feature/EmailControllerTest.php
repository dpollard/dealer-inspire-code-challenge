<?php

namespace Tests\Feature;

use App\Email;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmailControllerTest extends TestCase
{
    use RefreshDatabase;

    public $message = [];

    public function setUp()
    {
        parent::setUp();
        Mail::getSwiftMailer()->registerPlugin(new TestingMailer($this));
    }

    /** @test */
    public function it_can_display_the_contact_page()
    {
        $this->get("/")
            ->assertSee("Dealer Inspire Code Challenge")
            ->assertStatus(200);
    }

    /** @test */
    public function it_can_verify_user_input_data()
    {
        $badFields = [
            'sender_email' => null,
            'sender_name' => null,
            'body' => null
        ];
        $badEmail = factory(Email::class)->make($badFields);
        $this->json('POST', '/api/send-email', $badEmail->toArray())
            ->assertStatus(422)
            ->assertJsonValidationErrors(array_keys($badFields));
    }

    /** @test */
    public function it_can_save_email_and_send_to_env_defined_owner()
    {
        $goodEmail = factory(Email::class)->make();
        $this->json('POST', '/api/send-email', $goodEmail->toArray())
            ->assertStatus(200)
            ->assertJson(['success' => true])
            ->assertJsonStructure(['success', 'message']);
        $this->seeEmailSentProperly($goodEmail);
    }

    public function seeEmailSentProperly(Email $goodEmail)
    {
        $this->assertNotEmpty($this->message);
        $this->assertEquals(array_keys($this->message->getTo())[0], env("OWNERS_EMAIL"));
        $this->assertEquals($this->message->getBody(), $goodEmail->body);
    }
}

class TestingMailer implements \Swift_Events_EventListener
{
    private $context;

    public function __construct($context)
    {
        $this->context = $context;
    }

    /**
     * @param $event
     */
    public function beforeSendPerformed(\Swift_Events_SendEvent $event)
    {
        $this->context->message = $event->getMessage();
    }
}