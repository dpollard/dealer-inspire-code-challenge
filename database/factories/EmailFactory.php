<?php

use Faker\Generator as Faker;

/** @var $factory \Illuminate\Database\Eloquent\Factory */
$factory->define(App\Email::class, function (Faker $faker) {
    return [
        'sender_name'  => $faker->name(),
        'sender_email' => $faker->email,
        'sender_phone' => $faker->phoneNumber,
        'body'         => $faker->paragraph
    ];
});
